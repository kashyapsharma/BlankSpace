package blankspace.S;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button add;
    private TextView total;
    private LinearLayout parent;
    String data="{}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        Bundle extras=getIntent().getExtras();
        if(extras!=null){
            data=extras.getString("data");
            try {
                JSONObject jsonObject=new JSONObject(data);
                if(!TextUtils.isEmpty(jsonObject.getString("name"))){
                    TextView name=new TextView(this);
                    name.setText("Name :"+jsonObject.getString("name"));
                    name.setTextSize(20);
                    name.setTextColor(Color.parseColor("#000000"));
                    parent.addView(name);
                }
                if(!TextUtils.isEmpty(jsonObject.getString("age"))){
                    TextView name=new TextView(this);
                    name.setText("Age :"+jsonObject.getString("age"));
                    parent.addView(name);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void initView() {
        add = (Button) findViewById(R.id.add);
        total = (TextView) findViewById(R.id.total);
        parent = (LinearLayout) findViewById(R.id.parent);

        add.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                Intent intent=new Intent(this,Fill.class);
                startActivity(intent);
                break;
        }
    }
}
