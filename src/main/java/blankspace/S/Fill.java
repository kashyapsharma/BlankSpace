package blankspace.S;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Fill extends AppCompatActivity implements View.OnClickListener {

    private Switch switche;
    private Button done;
    private LinearLayout parent;
    ArrayList<Integer> edits= new ArrayList<>();
    ArrayList<Integer> edits1= new ArrayList<>();
    ArrayList<EditText> ed= new ArrayList<>();
    ArrayList<Spinner> sps= new ArrayList<>();
    EditText eds;
    TextView tv;
    Spinner sp;
    int min=0;
    int max=100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill);
        initView();
    }

    private void initView() {
        switche = (Switch) findViewById(R.id.switche);
        done = (Button) findViewById(R.id.done);
        parent = (LinearLayout) findViewById(R.id.parent);
        done.setOnClickListener(this);
        JSONArray jsonArray2= null;
        try {
            jsonArray2 = new JSONArray("[\n" +
                    "{'field-name':'name', 'type':'text', 'required':true},\n" +
                    "{'field-name':'age', 'type':'number', 'min':18, 'max':65},\n" +
                    "{'field-name':'address', 'type':'multiline'}\n" +
                    "]\n");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        changeJson(jsonArray2);


        switche.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        JSONArray jsonArray1= null;
                        try {
                            jsonArray1 = new JSONArray("[\n" +
                                    "{'field-name':'name', 'type':'text'},\n" +
                                    "{'field-name':'age', 'type':'number'},\n" +
                                    "{'field-name':'gender', 'type':'dropdown', 'options':['male', 'female', 'other']},\n" +
                                    "{'field-name':'address', 'type':'multiline'}\n" +
                                    "]");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        changeJson(jsonArray1);
                    }else{
                        JSONArray jsonArray2= null;
                        try {
                            jsonArray2 = new JSONArray("[\n" +
                                    "{'field-name':'name', 'type':'text', 'required':true},\n" +
                                    "{'field-name':'age', 'type':'number', 'min':18, 'max':65},\n" +
                                    "{'field-name':'address', 'type':'multiline'}\n" +
                                    "]\n");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        changeJson(jsonArray2);
                    }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                submit();
                break;
        }
    }

    private void submit() {

        JSONObject js=new JSONObject();
        for(int i=0;i<ed.size();i++){
            for(int m=0; m<edits.size();m++){
                if(edits.get(m)==ed.get(i).getId()){
                    if(TextUtils.isEmpty(ed.get(i).getText().toString().trim())){
                        Toast.makeText(getBaseContext(),ed.get(i).getHint()+" field can not be empty.",Toast.LENGTH_LONG).show();
                        return;
                    }
                }
            }
            for(int n=0; n<edits1.size();n++){
                if(edits1.get(n)==ed.get(i).getId()){
                    try {
                        if(Integer.parseInt(ed.get(i).getText().toString())<min){
                            Toast.makeText(getBaseContext(),"Invalid age.",Toast.LENGTH_LONG).show();
                            return;
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                js.put(ed.get(i).getHint()+"",ed.get(i).getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for(int k=0;k<sps.size();k++){
            try {
                js.put(sps.get(k).getPrompt()+"",sps.get(k).getSelectedItem());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Toast.makeText(getBaseContext(),js.toString(),Toast.LENGTH_LONG).show();
        Intent intent =new Intent(this, MainActivity.class);
        intent.putExtra("data",js.toString());
        startActivity(intent);
    }

    private void changeJson(JSONArray jsonArray){
            parent.removeAllViews();
            sps.clear();
            ed.clear();
            edits.clear();
            edits1.clear();
            for(int i=0;i<jsonArray.length();i++){

                try {
                    if (jsonArray.getJSONObject(i).getString("type").equalsIgnoreCase("dropdown")) {
                        tv=new TextView(this);
                        tv.setAllCaps(true);
                        tv.setText(jsonArray.getJSONObject(i).getString("field-name"));
                        sp=new Spinner(this);
                        sp.setPadding(5,5,5,5);
                        final int sdk = android.os.Build.VERSION.SDK_INT;
                        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            sp.setBackgroundDrawable( getResources().getDrawable(R.drawable.edb) );
                        } else {
                            sp.setBackground( getResources().getDrawable(R.drawable.edb));
                        }
                        sp.setId(jsonArray.length()+i);
                        sp.setPrompt(jsonArray.getJSONObject(i).getString("field-name"));
                        ArrayList<String> genders = new ArrayList<String>();
                        JSONArray jsonArray1=  jsonArray.getJSONObject(i).getJSONArray("options");

                        for(int k=0;k<jsonArray1.length();k++)
                        {
                            genders.add(jsonArray1.getString(k));
                        }
                        ArrayAdapter<String> genderadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genders);
                        genderadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sp.setAdapter(genderadapter);
                        parent.addView(tv);
                        parent.addView(sp);
                        sps.add(sp);
                    }
                    else {
                        eds=new EditText(this);
                        tv=new TextView(this);
                        eds.setHintTextColor(Color.TRANSPARENT);
                        eds.setPadding(5,5,5,5);
                        final int sdk = android.os.Build.VERSION.SDK_INT;
                        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            eds.setBackgroundDrawable( getResources().getDrawable(R.drawable.edb) );
                        } else {
                            eds.setBackground( getResources().getDrawable(R.drawable.edb));
                        }

                        eds.setId(i);
                        tv.setText(jsonArray.getJSONObject(i).getString("field-name"));
                        tv.setAllCaps(true);
                        eds.setHint(jsonArray.getJSONObject(i).getString("field-name"));
                        if(jsonArray.getJSONObject(i).getString("type").equalsIgnoreCase("text")){
                            eds.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                        }
                        else if(jsonArray.getJSONObject(i).getString("type").equalsIgnoreCase("number")){
                            eds.setInputType(InputType.TYPE_CLASS_NUMBER);
                            try {
                                min=jsonArray.getJSONObject(i).getInt("min");
                                edits1.add(eds.getId());
                                max=jsonArray.getJSONObject(i).getInt("max");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            eds.setFilters(new InputFilter[]{new InputFilterMinMax(min, max)});
                        }
                        else if(jsonArray.getJSONObject(i).getString("type").equalsIgnoreCase("multiline")){
                            eds.setSingleLine(false);
                            eds.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                        }
                        try {
                            if(jsonArray.getJSONObject(i).getBoolean("required")){
                                edits.add(eds.getId());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        parent.addView(tv);
                        parent.addView(eds);
                        ed.add(eds);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
    }
}
